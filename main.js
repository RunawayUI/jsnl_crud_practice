const requestUrl = 'https://jsonplaceholder.typicode.com/posts';
const postsWrapper = document.querySelector('.posts-wrapper');
let posts = [];
let btnDelete;
const btnCreate = document.getElementById('btn-create');
const btnUpdate = document.getElementById('btn-update');

const createTemplate = data => {
    return `
        <div class="post" data-id="${data.id}">
            <div class="id">ID: ${data.id}</div>
            <div class="title">TITLE: ${data.title}</div>
            <div class="body">BODY: ${data.body}</div>
            <button class="btn-delete">Delete Post</button>
        </div>
    `
}

const getPosts = url => {
    fetch(url)
        .then(response => response.json())
        .then(json => {
            posts = json;
            if (posts) {
                posts.filter(item => {
                    return item.id >= 40 && item.id <= 42
                }).forEach(post => {
                    postsWrapper.innerHTML += createTemplate(post);
                });
                btnDelete = document.querySelectorAll('.btn-delete');
            }
        })
        .then(() => {
            for (let elem of btnDelete) {
                elem.addEventListener('click', e => {
                    let idElem = e.target.parentNode.dataset.id;
                    deletePost(requestUrl, idElem);
                })
            }
        })
}
getPosts(requestUrl);

const deletePost = (url, id) => {
    fetch(`${url}/${id}`, {
        method: 'DELETE'
    })
}

const createPost = url => {
    const inputTitle = document.getElementById('title').value,
          inputBody = document.getElementById('body').value,
          inputUserId = document.getElementById('userId').value;

    const inputObj = {
        title: inputTitle,
        body: inputBody,
        userId: inputUserId,
    }

    fetch(url, {
        method: 'POST',
        body: JSON.stringify(inputObj),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
    .then((response) => response.json())
    .then((json) => console.log(json));
}

btnCreate.addEventListener('click', e => {
    e.preventDefault();
    createPost(requestUrl);
})


const updatePost = url => {
    const inputTitleUpdate = document.getElementById('titleUpdate').value,
          inputBodyUpdate = document.getElementById('bodyUpdate').value,
          inputUserIdUpdate = document.getElementById('userIdUpdate').value,
          inputIdUpdate = document.getElementById('idUpdate').value;

    const inputObj = {
        id: inputIdUpdate,
        title: inputTitleUpdate,
        body: inputBodyUpdate,
        userId: inputUserIdUpdate,
    }

    fetch(url + '/' + inputIdUpdate, {
        method: 'PUT',
        body: JSON.stringify(inputObj),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
    })
    .then((response) => response.json())
    .then((json) => console.log(json));
}

btnUpdate.addEventListener('click', e => {
    e.preventDefault();
    updatePost(requestUrl);
})